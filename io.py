#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue, May 30th 2023
Written during WIN FMRIB/OHBA Analysis groups' PyPhyPred hackathon
#add names   
A separate copy of the licence is also included within the repository.

This file includes helper functions for reading from / writing to disk.
 
"""
import scipy as sp
import numpy as np
import pandas as pd
import os.path

###############################################################################

#import python packages that you need here; e.g. numpy scipy etc

###############################################################################

def load_file(filename, fileformat='', var_names='', subj_ids='', **kwargs):
    # This function takes a filename and loads it into a numpy array.
    # If the fileformat is not user-specified, it will be determined 
    # automatically based on filename extension. If `var_names` is missing
    # default ones will be created as `v001`, `v002`, etc. If `subj_ids` is
    # missing, default ones will be created as `s001`, `s002`, etc.
    #
    # Supported file formats:    
    #    
    # .txt
    #    TXT file can only contain numeric values, no header row; variable 
    #    must be set with `var_names` argument, and subject IDs with 
    #    `subj_ids` argument.
    # .csv
    #    CSV files are expected to have a header row with variable names, 
    #    and first column must be subject IDs; header row can be omitted
    #    if `var_names` is given, and first column will be treated as any 
    #    other variable if `subj_ids` is given.
    # .mat 
    #    MAT file must represent a single 2D array, represented as either
    #     - MATLAB a single 2D numeric array; variable must be set with 
    #       `var_names` argument, and subject IDs with `subj_ids` argument.
    #     - MATLAB table object, with named columns giving variable names
    #       and named rows giving subject IDs. [TODO - if desired]
    # .npy
    # .npz
    #    Python numpy array; variable must be set with `var_names` argument,
    #    and subject IDs with `subj_ids` argument.
    #
    # For all input formats, the convention is
    # rows - Different individuals
    # cols - Different variables
    # 

    # Check if file exists
    if not os.path.isfile(filename):
        raise ValueError('File not found: {}'.format(filename))
    
    # Determine file format if not specified
    if fileformat == '':
        fileformat = os.path.splitext(filename)[1]
        
    # Load file
    if fileformat == '.txt':
        # Load TXT file
        output_array = np.loadtxt(filename)
    elif fileformat == '.csv':
        # Load CSV file
        if var_names == '':
            # Read variable names from header row
            var_names = np.loadtxt(filename, delimiter=',', max_rows=1, dtype=str)
            output_array = np.loadtxt(filename, delimiter=',', skiprows=1)
        else:
            output_array = np.loadtxt(filename, delimiter=',')
        if subj_ids == '':
            subj_ids = output_array[:,0]
            output_array = output_array[:,1:]
    elif fileformat == '.mat':
        # Load single-variable MAT file into a numpy array
        tmpmat = sp.io.loadmat(filename)[filename]
        # Check if MAT file is a single 2D array
        if tmpmat.dtype.names is None:
            # MAT file is a single 2D array
            output_array = tmpmat
        else:
            # MAT file is a table or mutiple variables
            raise NotImplementedError('MATLAB file not a single 2D array')
    elif fileformat == '.npy':
        # Load NPY file
        output_array = np.load(filename)
    elif fileformat == '.npz':
        # Load NPZ file
        output_array = np.load(filename)['arr_0']

    # Create default variable names if not specified
    if var_names == '':
        var_names = ['v{:03d}'.format(i+1) for i in range(output_array.shape[1])]

    # Create default subject IDs if not specified
    if subj_ids == '':
        subj_ids = ['s{:03d}'.format(i+1) for i in range(output_array.shape[0])]

    # Check of length of variable names matches number of columns
    if len(var_names) != output_array.shape[1]:
        raise ValueError('Number of variable names does not match number of columns')
    
    # Check if length of subject IDs matches number of rows
    if len(subj_ids) != output_array.shape[0]:
        raise ValueError('Number of subject IDs does not match number of rows')
    
    # Convert numpy array to Pandas dataframe
    output_array = pd.DataFrame(output_array, index=subj_ids, columns=var_names)

    return output_array


def save_file(filename, fileformat='', **kwargs):
    #this function takes a filename and loads it into a numpy array
    #if the fileformat is not user-specified, determine it automatically based on filename extension
    #throw an error if file format is not supported
    #example formats that could be good to support: .npy, .npz, .txt, .csv, .mat 
    return #output_array